package com.example.nikit.tutu_app;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.codetroopers.betterpickers.datepicker.DatePickerBuilder;
import com.example.nikit.tutu_app.model.City;
import com.example.nikit.tutu_app.model.Station;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.apache.commons.io.IOUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainFragment extends Fragment {
    private OnFragmentInteractionListener mListener;
    List<City> data;
    EditText editText;
    CityAdapter mAdapter;


    public MainFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        EditText editText = (EditText) view.findViewById(R.id.editText);

        final SlidingUpPanelLayout panel = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);
        final TextView stationTitle = (TextView) view.findViewById(R.id.stationTitle);
        final TextView cityTitle = (TextView) view.findViewById(R.id.cityTitle);
        final TextView regionTitle = (TextView) view.findViewById(R.id.regionTitle);
        final TextView districtTitle = (TextView) view.findViewById(R.id.districtTitle);

        Button datePick = (Button) view.findViewById(R.id.datePickBtn);
        datePick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerBuilder dpb = new DatePickerBuilder()
                        .setFragmentManager(getFragmentManager())
                        .setStyleResId(R.style.BetterPickersDialogFragment)
                        .setTargetFragment(MainFragment.this);
                dpb.show();
            }
        });

        Spinner to = (Spinner) view.findViewById(R.id.to);
        Spinner from = (Spinner) view.findViewById(R.id.from);
        Gson gson = new Gson();
        AssetManager am = getActivity().getAssets();
        try {
            InputStream inputStream = am.open("allStations.json");
            String total = IOUtils.toString(inputStream, "UTF-8");
            JsonObject jsonObject = gson.fromJson(total, JsonObject.class);

            Type t = new TypeToken<List<City>>() {
            }.getType();
            data = (List<City>) new Gson().fromJson(jsonObject.getAsJsonArray("citiesFrom"), t);
            data.addAll((List<City>) new Gson().fromJson(jsonObject.getAsJsonArray("citiesTo"), t));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



        mAdapter = new CityAdapter(getActivity().getApplicationContext(), data, new CityAdapter.OnItemClickListener() {
            @Override
            public void onClick(Station st) {
                panel.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                stationTitle.setText(st.getStationTitle());
                cityTitle.setText(st.getCityTitle());
                regionTitle.setText(st.getRegionTitle());
                districtTitle.setText(st.getDistrictTitle());
            }
        });
        mAdapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
            @Override
            public void onListItemExpanded(int position) {
                City expandedCity = data.get(position);
            }

            @Override
            public void onListItemCollapsed(int position) {
                City collapsedCity = data.get(position);
            }
        });

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                mAdapter.getFilter().filter(s.toString());
                mAdapter.notifyDataSetChanged();
            }
        });

        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}

package com.example.nikit.tutu_app.model;

import java.io.Serializable;

/**
 * Created by nikit on 06.09.2016.
 */
public class Point implements Serializable {
    private String longitude;
    private String latitude;

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
}

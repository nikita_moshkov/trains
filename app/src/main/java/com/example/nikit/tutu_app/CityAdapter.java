package com.example.nikit.tutu_app;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.example.nikit.tutu_app.model.City;
import com.example.nikit.tutu_app.model.Station;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by nikit on 04.09.2016.
 */
public class CityAdapter extends ExpandableRecyclerAdapter<CityViewHolder, StationViewHolder> implements Filterable {

    private LayoutInflater mInflator;
    private StationFilter filter;
    private List<City> allModelItemsArray;
    private List<City> filteredModelItemsArray;
    private List<? extends ParentListItem> parentItemList;
    private OnItemClickListener listener;

    public CityAdapter(Context context, @NonNull List<? extends ParentListItem> parentItemList, OnItemClickListener listener) {
        super(parentItemList);
        this.listener = listener;
        this.parentItemList = parentItemList;
        mInflator = LayoutInflater.from(context);
        allModelItemsArray = new ArrayList<>();
        allModelItemsArray.addAll((List<City>) parentItemList);
        filteredModelItemsArray = new ArrayList<>();
        this.filteredModelItemsArray = new ArrayList<>();
        filteredModelItemsArray.addAll(allModelItemsArray);
        getFilter();
    }

    public interface OnItemClickListener {
        void onClick(Station Item);
    }

    @Override
    public CityViewHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View cityView = mInflator.inflate(R.layout.city_view, parentViewGroup, false);
        return new CityViewHolder(cityView);
    }

    @Override
    public StationViewHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View stationView = mInflator.inflate(R.layout.station_view, childViewGroup, false);
        return new StationViewHolder(stationView);
    }

    @Override
    public void onBindParentViewHolder(CityViewHolder cityViewHolder, int position, ParentListItem parentListItem) {
        City city = (City) parentListItem;
        cityViewHolder.bind(city);
    }

    @Override
    public void onBindChildViewHolder(StationViewHolder stationViewHolder, int position, Object childListItem) {
        Station station = (Station) childListItem;
        stationViewHolder.click(station, listener);
        stationViewHolder.bind(station);
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new StationFilter();
        }
        return filter;
    }

    public void refresh(List<? extends ParentListItem> cities) {
        //this.parentItemList.clear();
    }

    private class StationFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (constraint.toString().length() > 0) {
                ArrayList<City> filteredItems = new ArrayList<City>();

                for (int i = 0, l = allModelItemsArray.size(); i < l; i++) {
                    List<Station> stations = allModelItemsArray.get(i).getStations();
                    for (int j = 0; j < stations.size(); j++) {
                        String m = stations.get(j).getStationTitle();
                        if (m.toLowerCase().contains(constraint))
                            filteredItems.add(allModelItemsArray.get(i));
                    }
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            } else {
                synchronized (this) {
                    result.values = allModelItemsArray;
                    result.count = allModelItemsArray.size();
                }
            }
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredModelItemsArray = (List<City>) results.values;
            refresh(filteredModelItemsArray);
        }
    }

}
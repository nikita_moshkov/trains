package com.example.nikit.tutu_app;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.example.nikit.tutu_app.model.Station;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

/**
 * Created by nikit on 04.09.2016.
 */
public class StationViewHolder extends ChildViewHolder {

    private TextView mIngredientTextView;
    private SlidingUpPanelLayout slidingUpPanelLayout;

    public StationViewHolder(View itemView) {
        super(itemView);
        mIngredientTextView = (TextView) itemView.findViewById(R.id.stationTv);
    }



    public void click(final Station station, final CityAdapter.OnItemClickListener listener) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(station);
            }
        });
    }



    public void bind(Station station) {
        mIngredientTextView.setText(station.getStationTitle());
    }

}